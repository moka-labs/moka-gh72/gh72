# A program for investigating Moka GitHub Issue 72

- [Memory utilization grows significantly beyond max_capacity() under load (#72)][gh-issue-72]

[gh-issue-72]: https://github.com/moka-rs/moka/issues/72


## Supported platforms

- Linux, x86_64 and AArch64
- macOS, Apple silicon (arm64) and Intel (x86_64)

Mostly tested on Linux x86_64 and macOS Apple silicon.

Windows MSVC is not supported as this program is using [jemalloc] as the
global allocator.

[jemalloc]: http://jemalloc.net/


## Cloning the repositories

```console
$ mkdir moka-gh72
$ cd moka-gh72

$ git clone -b gh72 https://gitlab.com/moka-labs/moka-gh72/moka-cht.git
$ git clone -b gh72 https://gitlab.com/moka-labs/moka-gh72/moka.git
$ git clone https://gitlab.com/moka-labs/moka-gh72/gh72.git
```


## Running

From a terminal, build and run this program:

```console
$ cd gh72
$ cargo run --release
```

Then, from another terminal, generate workloads like the followings:

```console
# Insert entries: (They will have no duplicate keys)
$ wrk -d 300 -t 8 -c 100 http://localhost:8080/v1/test/moka/leak

# Invalidate all entries:
$ curl http://localhost:8080/v1/test/moka/clear
```

The program will print the following information in CSV format:

- Cache statistics:
    - Entry Count
    - Weighted Size
- Allocator statistics (jemalloc):
    - Resident Bytes. (Roughly matches to `RSS` value from `top` command)
    - Allocated Bytes `(A)`. (The amount of memory currently used by the application.
      Obtained by `jemalloc`'s stats)
- Estimation
    - Estimated Allocation Bytes `(B)`. (Calculated by the stats of internal data
      structures below. See the function `estimated_allocation_bytes` in [`src/main.rs`][src-main])
    - Difference in Allocation Bytes: `(A) - (B)`
- Statistics of some key internal data structures:
    - Frequency Sketch Size (in bytes). The size of the population estimator. (aka LFU filter)
    - HashMap Capacity (in number of entries)
    - HashMap &mdash; `BucketArray`:
        - Creation Count
        - Allocated Bytes
        - Drop Count
        - Released Bytes
    - HashMap &mdash; `Bucket`:
        - Creation Count
        - Drop Count
    - Cache &mdash; `ValueEntry`:
        - Creation Count
        - Drop Count
    - Cache &mdash; `EntryInfo`:
        - Creation Count
        - Drop Count
    - Cache &mdash; Access Order Queue Node:
        - Creation Count
        - Drop Count
    - Cache &mdash; Write Order Queue Node:
        - Creation Count
        - Drop Count

[src-main]: https://gitlab.com/moka-labs/moka-gh72/gh72/-/blob/main/src/main.rs

## Changing the cache configuration

Please directly edit [`src/main.rs`][src-main].
For example, you can change the cache capacity by editing the following line:

```rust
const CACHE_MAX_CAPACITY: u64 = 64 * 1024 * 1024;
```


## Credits

The original program was provided by Chris M. [@chriskyndrid][gh-chris].

[gh-chris]: https://github.com/chriskyndrid


## License 

The MIT License.
