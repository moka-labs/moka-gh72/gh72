use std::time::Duration;

use actix_web::{get, web, App, HttpResponse, HttpServer, Scope};
use moka::future::Cache;
use scheduled_thread_pool::ScheduledThreadPool;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use uuid::Uuid;

#[cfg(not(target_env = "msvc"))]
use tikv_jemallocator::Jemalloc;

#[cfg(not(target_env = "msvc"))]
#[global_allocator]
static GLOBAL: Jemalloc = Jemalloc;

#[cfg(target_env = "msvc")]
compile_error!("Sorry, Windows MSVC target is not supported because we cannot use jemalloc there");

// const CACHE_MAX_CAPACITY: u64 = 256 * 1024 * 1024;
const CACHE_MAX_CAPACITY: u64 = 64 * 1024 * 1024;
// const CACHE_MAX_CAPACITY: u64 = 8 * 1024 * 1024;

pub type DataCache = Cache<Uuid, Vec<u8>>;

pub fn api_config_v1(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/v1").service(api_config_v1_members()));
}

pub fn api_config_v1_members() -> Scope {
    web::scope("/test").service(web::scope("/moka").service(leak).service(clear))
}

pub struct AppData {
    cache: DataCache,
}

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    let cache = Cache::builder()
        .weigher(|_key, value: &Vec<u8>| -> u32 { value.len().try_into().unwrap_or(u32::MAX) })
        .max_capacity(CACHE_MAX_CAPACITY)
        .build();

    // `cache.show_sizes` will print the followings:
    //     Vec<u8>: 24
    //     ValueEntry: 64
    //     EntryInfo: 16
    //     Weighted: 24
    //     DeqNode<KeyHashDate>: 56
    //     DeqNode<KeyDate>: 48
    //     Mutex<DeqNodes>: 24
    //
    // cache.show_sizes();
    // println!();

    print_stats_header();

    let thread_pool = ScheduledThreadPool::new(1);
    {
        let cache = cache.clone();
        thread_pool.execute_at_fixed_rate(
            Duration::from_secs(1),
            Duration::from_secs(5),
            move || {
                print_stats(&cache);
            },
        );
    }

    let data = web::Data::new(AppData { cache });

    let app = HttpServer::new(move || App::new().app_data(data.clone()).configure(api_config_v1))
        .bind("127.0.0.1:8080")?;

    app.run().await?;

    Ok(())
}

#[get("/leak")]
pub async fn leak(data: web::Data<AppData>) -> HttpResponse {
    let cache_item = TestStruct {
        name: "Some name".to_owned(),
    };
    let id = Uuid::new_v4();
    let _ = set_data::<TestStruct>(&data.cache, &id, &cache_item).await;
    let _ = get_data::<TestStruct>(&data.cache, &id).await;

    let response = TestResponse { complete: true };

    HttpResponse::Ok()
        .content_type("application/json")
        .body(serde_json::to_string(&response).unwrap())
}

#[get("/clear")]
pub async fn clear(data: web::Data<AppData>) -> HttpResponse {
    data.cache.invalidate_all();
    let response = TestResponse { complete: true };

    HttpResponse::Ok()
        .content_type("application/json")
        .body(serde_json::to_string(&response).unwrap())
}

#[derive(Eq, PartialEq, Clone, Debug, Serialize, Deserialize)]
pub struct TestStruct {
    name: String,
}

#[derive(Serialize)]
pub struct TestResponse {
    complete: bool,
}

pub async fn set_data<T>(cache: &DataCache, key: &Uuid, cache_data: &T) -> anyhow::Result<bool>
where
    T: Send + Sync + Serialize,
{
    use bincode::{config::standard, serde::encode_to_vec};

    let encoded = encode_to_vec(&cache_data, standard())?;
    cache.insert(*key, encoded).await;
    Ok(true)
}

pub async fn get_data<T>(cache: &DataCache, key: &Uuid) -> anyhow::Result<T>
where
    T: Send + Sync + DeserializeOwned,
{
    use bincode::{config::standard, serde::decode_from_slice};

    let cache_item = match cache.get(key) {
        Some(item) => item,
        None => anyhow::bail!("Cache item not found"),
    };

    let (decoded, _len): (T, usize) = decode_from_slice(&cache_item[..], standard())?;

    Ok(decoded)
}

fn print_stats_header() {
    let header = "Entry Count,Weighted Size,\
        Resident Bytes,Allocated Bytes,\
        Estimated Allocation Bytes,Diff,\
        Freq Sketch Size [bytes],HashMap Capacity [entries],\
        BucketArray Creation Count,Allocated Bytes,Drop Count,Released Bytes,\
        Bucket Creation Count,Drop Count,\
        ValueEntry Creation Count,Drop Count,EntryInfo Creation Count,Drop Count,\
        Access Order Q Node Creation Count,Drop Count,\
        Write Order Q Node Creation Count,Drop Count";

    println!("{}", header);
}

fn print_stats(cache: &DataCache) {
    use tikv_jemalloc_ctl::{epoch, stats};

    let e = epoch::mib().unwrap();
    e.advance().unwrap();
    let resident = stats::resident::read().unwrap();
    let allocated = stats::allocated::read().unwrap();

    let cache_stats = cache.stats();
    let g_stats = moka::GlobalStats::current();
    let cht_stats = moka_cht::stats::GlobalStats::current();

    let estimation = estimated_allocation_bytes(&cache_stats, &g_stats, &cht_stats);

    println!(
        "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}",
        cache_stats.estimated_entry_count,
        cache_stats.weighted_size,
        resident,
        allocated,
        estimation,
        (allocated as i64 - estimation as i64),
        cache_stats.freq_sketch_size,
        cache_stats.hashmap_size,
        cht_stats.bucket_array_creation_count,
        cht_stats.bucket_array_allocation_bytes,
        cht_stats.bucket_array_drop_count,
        cht_stats.bucket_array_release_bytes,
        cht_stats.bucket_creation_count,
        cht_stats.bucket_drop_count,
        g_stats.value_entry_creation_count,
        g_stats.value_entry_drop_count,
        g_stats.entry_info_creation_count,
        g_stats.entry_info_drop_count,
        g_stats.aoq_node_creation_count,
        g_stats.aoq_node_drop_count,
        g_stats.woq_node_creation_count,
        g_stats.woq_node_drop_count,
    );

    fn estimated_allocation_bytes(
        cache_stats: &moka::CacheStats,
        g_stats: &moka::GlobalStats,
        cht_stats: &moka_cht::stats::GlobalStats,
    ) -> u64 {
        // Byte sizes for 64-bit platforms
        const KEY_SIZE: u64 = 16;
        const VALUE_SIZE: u64 = 34;
        const HASHMAP_BUCKET_SIZE: u64 = 16;
        const KEY_ARC_OVERHEAD: u64 = 16;
        const VALUE_ENTRY_SIZE: u64 = 56;
        const ENTRY_INFO_SIZE: u64 = 40;
        const AOQ_NODE_SIZE: u64 = 56;
        const WOQ_NODE_SIZE: u64 = 48;

        cache_stats.freq_sketch_size
            + (cht_stats.bucket_array_allocation_bytes - cht_stats.bucket_array_release_bytes)
            + (HASHMAP_BUCKET_SIZE + KEY_SIZE + KEY_ARC_OVERHEAD)
                * (cht_stats.bucket_creation_count - cht_stats.bucket_drop_count)
            + VALUE_SIZE * cache_stats.estimated_entry_count
            + VALUE_ENTRY_SIZE
                * (g_stats.value_entry_creation_count - g_stats.value_entry_drop_count)
            + ENTRY_INFO_SIZE * (g_stats.entry_info_creation_count - g_stats.entry_info_drop_count)
            + AOQ_NODE_SIZE * (g_stats.aoq_node_creation_count - g_stats.aoq_node_drop_count)
            + WOQ_NODE_SIZE * (g_stats.woq_node_creation_count - g_stats.woq_node_drop_count)
    }
}
